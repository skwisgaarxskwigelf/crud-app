from flask import render_template
from flask_login import login_required

from . import home


@home.route('/')
def homepage():
    return render_template('home/index.html')


@home.route('/dash')
@login_required
def dash():
    return render_template('home/dash.html')

